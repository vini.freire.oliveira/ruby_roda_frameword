# Encoding: utf-8
require 'rubygems'
require 'bundler'

Bundler.require

# Setup roda frameword
require 'roda'
require 'rack/unreloader'

# Front-end
# require 'tilt'

#Api

# Setup database
require 'mongoid'
require 'roar/json/hal'
require 'rack/conneg'

# Save applicaction app path
app = File.expand_path('../', __FILE__)
$LOAD_PATH.unshift(app) unless $LOAD_PATH.include? app

# Set Rack
unreloader ||= Rack::Unreloader.new(subclasses: %w'Roda', reload: ENV['RACK_ENV']){App}
unreloader.require('app/api.rb'){'App'}  #set as api framework

#Mongoid.load!("config/database.yml", settings.environment)


# Start monolitic version
# require 'app/application'

# run(ENV['RACK_ENV'] ? unreloader : Application.freeze.app)

# Start api version
require 'app/api'

run(ENV['RACK_ENV'] ? unreloader : Api.freeze.app)
