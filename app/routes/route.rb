class Application
  hash_branch 'route' do |r|
    r.on 'hello' do
      r.get do
        @title = "Hello World"
        view 'index'
      end
    end
  end
end
